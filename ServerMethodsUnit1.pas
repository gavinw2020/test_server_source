unit ServerMethodsUnit1;

interface

uses
  System.SysUtils, System.Classes, System.Json,
  DataSnap.DSProviderDataModuleAdapter, Datasnap.DSServer, Datasnap.DSAuth;

type
  TServerMethods1 = class(TDSServerModule)
  private
    { Private declarations }
  public
    { Public declarations }
    { get }
    function User(value: string): string;
    { post }
    function updateUser(value: string; Obj: TJSONObject): string;
    { delete }
    function cancelUser(value: string): string;
    { put }
    function acceptUser(value: string; Obj: TJSONObject): string;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses
  System.StrUtils;

function TServerMethods1.User(value: string): string;
begin
  Result := '[查询数据] GET ' + value;
end;

function TServerMethods1.updateUser(value: string; Obj: TJSONObject): string;
begin
  Result := '[插入数据] POST ' + value;
end;

function TServerMethods1.cancelUser(value: string): string;
begin
  Result := '[删除数据] DELETE ' + value;
end;

function TServerMethods1.acceptUser(value: string; Obj: TJSONObject): string;
begin
  Result := '[修改数据] PUT ' + value;
end;

end.

